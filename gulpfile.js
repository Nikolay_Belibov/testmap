/* =======================================
 Variables
 ======================================= */
var Fontmin = require('fontmin'),
    gulp = require('gulp'),
    autoprefixer = require('gulp-autoprefixer'),
    jade = require('gulp-jade'),
    concat = require('gulp-concat'),
    notify = require('gulp-notify'),
    uglify = require('gulp-uglify'),
    plumber = require('gulp-plumber'),
    rename = require('gulp-rename'),
    sass = require('gulp-sass'),
    image = require('gulp-image');
/* ===========================================================================*/
/* =======================================
 PATH
 ======================================= */
var path = {
    /* ===========================
     Resourses files
     =========================== */
    src: {
        /* ===========================  Fonts   =========================== */
        fonts: {
            input: 'src/fonts/**/*.*',
            output: 'src/fonts/'
        },
        /* ===========================  Images files    =========================== */
        images: {
            //favicon
            favicon: 'src/images/favicon/*.*',
            // svg
            svg: '',
            //images
            images: [
                'src/images/*.png',
                'src/images/*.jpg',
                'src/images/*.gif'
            ]
        },
        /* ===========================  JavaScript files    =========================== */
        js: {
            // JavaScript core
            core: [
                'src/js/core/modernizr.js',
                'src/js/core/jquery.js'
            ],
            // JavaScript plugins, modules and outher js files
            components: [
                'src/js/components/**/*.js'
            ],
            // JavaScript init
            script: 'src/js/script.js'
        },
        /* ===========================  SCSS files  =========================== */
        scss: {
            // style.scss
            style: [
                'src/scss/style.scss',
                'src/scss/setings/**/*.scss',
                'src/scss/components/**/*.scss'
            ],
            // Other scss files
            other: 'src/scss/ie.scss',
            select2: ['src/scss/select2/core.scss']
        },
        /* ===========================  JADE Templates files    =========================== */
        templates: 'src/templates/**/*.jade'
    },
    /* ===========================
     After compilation files
     =========================== */
    build: {
        /* ===========================
         Development version
         =========================== */
        dev: {
            /* ===========================  Fonts   =========================== */
            fonts: 'build/dev/fonts/',
            /* ===========================  Images files    =========================== */
            images: {
                //favicon
                favicon: 'build/dev/images/favicon/',
                // svg
                svg: 'build/dev/images/svg/',
                //compress images
                images: 'build/dev/images/'
            },
            /* ===========================  JavaScript files    =========================== */
            js: {
                // JavaScript core
                core: 'build/dev/js/',
                // JavaScript plugins, modules and outher js files
                components: 'build/dev/js/',
                // JavaScript init
                script: 'build/dev/js/'
            },
            /* ===========================  Stylesheats files (CSS) =========================== */
            css: {
                style: 'build/dev/css/',
                other: 'build/dev/css/',
                select2: 'build/dev/css/'
            },
            /* ===========================  Jade file convert to THML =========================== */
            templates: 'build/dev/'
        },
        /* ===========================
         Production version
         =========================== */
        prod: {
            /* ===========================  Stylesheats files (CSS) =========================== */
            css: 'build/prod/css/'
        }
    }
};
/* ===========================================================================*/
/* =======================================
 SRC --> BUILD --> DEV
 ======================================= */
/* ===========================  Fonts =========================== */
gulp.task('dev-fontCreate', function () {
    var fontmin = new Fontmin()
        .src(path.src.fonts.input)
        .use(Fontmin.otf2ttf())
        .dest(path.src.fonts.output)
        .use(Fontmin.ttf2eot())
        .use(Fontmin.ttf2woff({deflate: true}))
        .dest(path.src.fonts.output);
    fontmin.run(function (err, files) {
        if (err) {
            throw err;
        }
        console.log(files[0]);
        // => { contents: <Buffer 00 01 00 ...> }
    });
});
gulp.task('dev-fontImport', function () {
    gulp.src(path.src.fonts.input)
        .pipe(notify("fonts import to (DEV) packge!"))
        .pipe(gulp.dest(path.build.dev.fonts));
});

/* ===========================  Images  =========================== */
gulp.task('dev-imageMin', function () {
    gulp.src(path.src.images.images)
        .pipe(image())
        .pipe(gulp.dest(path.build.dev.images.images));
});

//favicon
gulp.task('dev-imageFavicon', function () {
    gulp.src(path.src.images.favicon)
        .pipe(gulp.dest(path.build.dev.images.favicon))
});

/* ===========================  JavaScript files  =========================== */
//core js
gulp.task('dev-jsCore', function () {
    gulp.src(path.src.js.core)
        .pipe(plumber({
            errorHandler: notify.onError("Error: <%= error.message %>")
        }))
        .pipe(concat('core.js'))
        .pipe(plumber.stop())
        .pipe(notify("js-core (DEV) compile success!"))
        .pipe(gulp.dest(path.build.dev.js.core));
});

//core js
gulp.task('dev-jsComponents', function () {
    gulp.src(path.src.js.components)
        .pipe(plumber({
            errorHandler: notify.onError("Error: <%= error.message %>")
        }))
        .pipe(concat('components.js'))
        .pipe(plumber.stop())
        .pipe(notify("components.js (DEV) compile success!"))
        .pipe(gulp.dest(path.build.dev.js.core));
});

//init js
gulp.task('dev-jsScript', function () {
    gulp.src(path.src.js.script)
        .pipe(plumber({
            errorHandler: notify.onError("Error: <%= error.message %>")
        }))
        .pipe(plumber.stop())
        .pipe(notify("script.js (DEV) compile success!"))
        .pipe(gulp.dest(path.build.dev.js.script));
});

//all task js
gulp.task('dev-js', ['dev-jsCore', 'dev-jsComponents', 'dev-jsScript']);
/* ===========================  SCSS files  =========================== */
gulp.task('dev-sass', function () {
    gulp.src(path.src.scss.style)
        .pipe(plumber({
            errorHandler: notify.onError("Error: <%= error.message %>")
        }))
        .pipe(sass())
        .pipe(autoprefixer({
            browsers: ['> 1%', 'last 15 versions'],
            cascade: false
        }))
        .pipe(plumber.stop())
        .pipe(notify("style.scss (DEV) compiled success!"))
        .pipe(gulp.dest(path.build.dev.css.style));
});
//IE stylesheat
gulp.task('dev-sassIE', function () {
    gulp.src(path.src.scss.other)
        .pipe(plumber({
            errorHandler: notify.onError("Error: <%= error.message %>")
        }))
        .pipe(sass())
        .pipe(autoprefixer({
            browsers: ['> 1%', 'last 15 versions'],
            cascade: false
        }))
        .pipe(plumber.stop())
        .pipe(notify("ie.scss (DEV) compiled success!"))
        .pipe(gulp.dest(path.build.dev.css.other));
});

//select3
gulp.task('dev-sassSelect2', function () {
    gulp.src(path.src.scss.select2)
        .pipe(plumber({
            errorHandler: notify.onError("Error: <%= error.message %>")
        }))
        .pipe(sass())
        .pipe(autoprefixer({
            browsers: ['> 1%', 'last 15 versions'],
            cascade: false
        }))
        .pipe(rename('select2.css'))
        .pipe(plumber.stop())
        .pipe(notify("select2.css (DEV) compiled success!"))
        .pipe(gulp.dest(path.build.dev.css.select2));
});
/* ===========================  Templates files  =========================== */
gulp.task('dev-jade', function () {
    return gulp.src(path.src.templates)
        .pipe(plumber({
            errorHandler: notify.onError("Error: <%= error.message %>")
        }))
        .pipe(jade({
            pretty: '\t',
            //debug: true
            //locals: jadedata
        }))
        .pipe(notify("*.jade (DEV) compiled success!"))
        .pipe(gulp.dest(path.build.dev.templates));
});
/* ===========================================================================*/
//DEV version all tasks
gulp.task('dev', ['dev-js', 'dev-sass', 'dev-jade']);

/* =======================================
 SRC --> BUILD --> PROD
 ======================================= */


/*============= watch task =============*/
gulp.task('watch', function () {
    //fonts
    gulp.watch(path.src.fonts.input, ['dev-fontImport']);
    //images
    gulp.watch(path.src.images.images, ['dev-imageMin']);
    //js
    gulp.watch(path.src.js.core, ['dev-jsCore']);
    gulp.watch(path.src.js.components, ['dev-jsComponents']);
    gulp.watch(path.src.js.script, ['dev-jsScript']);
    //scss
    gulp.watch(path.src.scss.style, ['dev-sass']);
    gulp.watch(path.src.scss.other, ['dev-sassIE']);
    gulp.watch(path.src.scss.select2, ['dev-sassSelect2']);
    //jade
    gulp.watch(path.src.templates, ['dev-jade']);
});
/*============= default task =============*/
gulp.task('default', ['watch']);