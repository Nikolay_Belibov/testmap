jQuery.fn.exists = function () {
    return $(this).length;
};

/* map
 ========================================================*/
dojoConfig = {
    locale: "ru",
    parseOnLoad: true
};

var map;
require([
    "esri/map",
    "esri/dijit/Search",

    "esri/geometry/Point",
    "esri/symbols/SimpleMarkerSymbol", "esri/graphic",
    "dojo/_base/array", "dojo/dom-style", "dojox/widget/ColorPicker",

    //"extras/ClusterLayer",

    "dojo/domReady!"
], function (Map, Search, Point,
             SimpleMarkerSymbol, Graphic,
             arrayUtils,
             ClusterLayer) {

    map = new Map("testMap", {
        basemap: "streets",
        center: [32.005, 46.956],
        zoom: 14,
        minZoom: 6
    });

    map.on("load", mapLoaded);

    function mapLoaded() {
        var points = [
            [32.03, 46.961],[32.008, 46.962],
            [32.004, 46.966],[32.04, 46.9535]
        ];

        var iconPath = "M24.0,2.199C11.9595,2.199,2.199,11.9595,2.199,24.0c0.0,12.0405,9.7605,21.801,21.801,21.801c12.0405,0.0,21.801-9.7605,21.801-21.801C45.801,11.9595,36.0405,2.199,24.0,2.199zM31.0935,11.0625c1.401,0.0,2.532,2.2245,2.532,4.968S32.4915,21.0,31.0935,21.0c-1.398,0.0-2.532-2.2245-2.532-4.968S29.697,11.0625,31.0935,11.0625zM16.656,11.0625c1.398,0.0,2.532,2.2245,2.532,4.968S18.0555,21.0,16.656,21.0s-2.532-2.2245-2.532-4.968S15.258,11.0625,16.656,11.0625zM24.0315,39.0c-4.3095,0.0-8.3445-2.6355-11.8185-7.2165c3.5955,2.346,7.5315,3.654,11.661,3.654c4.3845,0.0,8.5515-1.47,12.3225-4.101C32.649,36.198,28.485,39.0,24.0315,39.0z";
        var initColor = "#1db2cf";
        arrayUtils.forEach(points, function(point) {
            var graphic = new Graphic(new Point(point), createSymbol(iconPath, initColor));
            map.graphics.add(graphic);
        });
    }

    function createSymbol(path, color){
        var markerSymbol = new esri.symbol.SimpleMarkerSymbol();
        markerSymbol.setPath(path);
        markerSymbol.setColor(new dojo.Color(color));
        markerSymbol.setOutline(null);
        return markerSymbol;
    }

    var search = new Search({
        map: map
    },"search");
    search.startup();



    // cluster layer that uses OpenLayers style clustering
    //clusterLayer = new ClusterLayer({
    //    "data": photoInfo.data,
    //    "distance": 100,
    //    "id": "clusters",
    //    "labelColor": "#fff",
    //    "labelOffset": 10,
    //    "resolution": map.extent.getWidth() / map.width,
    //    "singleColor": "#888",
    //    "singleTemplate": popupTemplate
    //});
});